from penduloSimple import *


if __name__=="__main__":
    print("ejecutando pendulo simple")

    g  = 9.8 #valor e la aceleración gravitacional  
    L = g/16 #valor de la longitud para la condicion impuesta sqrt(g/L) = 4
    theta = 1 # ang inicial
    omega0 = 0 #vel angulr inicial
    dt  = 0.1 #paso de tiempo
    tmax = 5 #tiempo maximo

#*****creamos una instancia con los parametros requeridos***********

    miPendulo=PenduloSimple(L,g,theta,omega0,dt,tmax)
    miPendulo.euler()
    miPendulo.desplazamientoAngular()