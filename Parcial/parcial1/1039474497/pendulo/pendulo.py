import numpy as np
import matplotlib.pyplot as plt

class PenduloBalistico:
    
    def __init__(self, length, mass_bullet, mass_block, bullet_velocity, gravity):
        """
        Inicializa los parámetros del péndulo balístico.
        """
        self.length = length
        self.mass_bullet = mass_bullet
        self.mass_block = mass_block
        self.bullet_velocity = bullet_velocity
        self.gravity = gravity
        self.time = np.arange(0, 100, 0.1)
    
    def calculate_deflection_angle(self):
        
        # Como vimos en clase, hay ciertas condiciones en las que la velocidad diverge y el pendulo se queda dando vueltas infintitamente,
        #por lo tanto hay que prevenir al usuario
        
        if self.bullet_velocity > (2 * np.sqrt(self.gravity * self.length) * (self.mass_bullet + self.mass_block)) / self.mass_bullet:
            raise ValueError('Cambiar parametros')
        else:
            deflection_angle = np.arccos(1 - (self.mass_bullet * self.bullet_velocity / (self.mass_bullet + self.mass_block)) ** 2 * (1 / (2 * self.gravity * self.length)))
            deflection_angle = np.rad2deg(deflection_angle)
            return print("el angulo de defleccion es", deflection_angle)
    
    def calculate_block_bullet_velocity(self):
        """
        Calcula la velocidad del sistema después del impacto.
        """
        block_bullet_velocity = np.sqrt(2 * self.gravity * self.length)
        return print("la rapidez de la bala es", block_bullet_velocity)

#Aplicamos herencia a la clase PenduloHijo, la cual calcula las condiciones de minimo
class PenduloHijo(PenduloBalistico):
    def __init__(self, length, mass_bullet, mass_block, bullet_velocity, gravity, theta):
        print('Inicializando clase de condiciones de minimo')
        super().__init__(length, mass_bullet, mass_block, bullet_velocity, gravity)
        self.theta = np.deg2rad(theta)

    
    def velPend(self):
        velMin = np.sqrt(5 * self.length * self.gravity ) #condicion de minimo pendulo
        print("La rapidez minima del sistema es", velMin)
        return velMin 

  
    def velBal(self):
        velMinm = 2 * (self.mass_bullet + self.mass_block) * self.velPend() / self.mass_bullet #condicion minimo masa m
        return print("la rapidez minima de la masa pequeña es ", velMinm)
    
    #el metodo calcularPeriodo se puede sobreescribir para calcular el periodo de cualquier pendulo
    def calcularPeriodo(self):
        T = 2*np.pi*np.sqrt(self.length / (self.gravity))
        return print("el periodo del pendulo es {}".format(T))
    
#Por el calculo con conservacion de energia mecanica se llega a que el sistema queda oscilando de manera armónica 

    def sol(self, t):
        return np.sin(t)

    # graficamos las oscilaciones
    def graficar(self):
        plt.title('movimiento angular del pendulo')
        plt.plot(self.time, self.sol(self.time))
        plt.savefig("grafica_pendulo.png")
        plt.xlabel('$t$')
        plt.ylabel('$θ(t)$')
     