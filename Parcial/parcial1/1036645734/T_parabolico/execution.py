from t_parabolico import TiroParabolico
from t_parabolico import TiroParabolicoFcc
import numpy as np
import matplotlib.pyplot as plt
from t_parabolico import t_parab_fricc_y


if __name__=="__main__":
    #instanciar clase (hacer un objeto)
    xi=0
    h=70
    v0=20
    ang=0
    g=9.8
    afr=1
    #se define el tiempo de vuelo
    t_vuelo=2*v0*np.sin(np.pi/180*ang)/g
    t = np.arange(0,t_vuelo,0.01)

    tp = np.arange(0,4.159451654038514,0.01)

    parabolica=TiroParabolico(xi,h,v0,ang,t) #instancia de la clase tiro parabolico inicial
    parabolicafrcc=TiroParabolicoFcc(xi,h,v0,ang,t,afr) #instancia de la clase tiro parabolico con friccion en x
    parabolicafrccv=t_parab_fricc_y(xi,h,v0,ang,tp,afr,vien_y=3) #instancia de la clase tiro parabolico con friccion vientos en y
 

    x_p=parabolica.viewposx()
    y_p=parabolica.viewposy()
    plt.plot(x_p,y_p, label='parábola sin fricción')


    x_pfr=parabolicafrcc.vfrx()
    x_p_vieny=parabolicafrccv.vfrx()
    y_p=parabolica.viewposy()
    y_p_vieny=parabolicafrccv.vfry()

    plt.plot(x_pfr,y_p, label='parábola con fricción')
    plt.plot(x_p_vieny,y_p_vieny, label='parábola con friccion de vientos en y')
    plt.xlabel("X(m)")
    plt.ylabel("Y(m)")
    plt.grid()
    plt.legend()
    plt.title("Tiro Parabólico")
    plt.show()
    plt.savefig("tiro_aprabolico.png")

    print(parabolica.t_vuelo())
    print(parabolicafrccv.t_vuelo())