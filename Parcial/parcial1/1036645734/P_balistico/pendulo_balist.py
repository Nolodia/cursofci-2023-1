


import numpy as np

class pendulo_balistico():
    #atributos que necesito para la solucion del problema
    def __init__(self, R, m, M,g,angulo):
        self.R=R
        self.m=m
        self.M=M
        self.angulo=angulo
        self.g=g

    #metodo para hallar la velocidad del bloque luego de la colision dado un angulo
    def vm(self): #velocidad masa bloque vala dado un angulo
        vm = np.sqrt(2*self.g*self.R*(1-np.cos(self.angulo)))
        return vm

    #metodo para hallar la velocidad inicial de la bala
    def vi(self):
        vi = self.vm()*(self.m+self.M)/self.m
        return vi
    
    def altura(self):
        altura = ((self.vi()**2)*self.m**2)/(self.g*2*(self.m+self.M)**2)
        return altura
    
    #se define la funcion que graficara el movimiento del pendulo balistico (la cual es un pendulo simple y describe un M.A.S)
    def theta(self):
        t = np.arange(0,10,0.01)
        omega = self.g/self.R #frecuencia natural del probelma
        #theta = np.arcsin(np.sin(omega*t))
        theta = 0.26*np.sin(omega*t) #la amplitud maxima esta dada por las pequeñas oscilaciones (15 grados)
        return theta


#se define el polimorfismo a POO

class pendulo_balistico_min_vuelta(pendulo_balistico):
    def __init__(self, R, m, M,g,angulo):
        super().__init__( R, m, M,g,angulo)
     
    def vi(self): #se define la fn para la vel_min para que de una vuleta completa dado un angulo
        vi = np.sqrt(5*self.g*self.R)*(self.m+self.M)/self.m
        return vi
    
    #polimorfismo altura (altura a la que llega la bala para que de una vuelta)
    def altura(self):
        altura = ((self.vi()**2)*self.m**2)/(self.g*2*(self.m+self.M)**2)
        return altura

