

# Importando clases.
from ClassTiroParabolico_P2N1_E1_FCI import TiroParabolico, TiroParabV2

if __name__ == '__main__': # Empezando la ejecución.
    # Configuración de atributos.
    velinit = 10 # Velocidad inicial.
    alpha = 42 # Ángulo de tiro.
    g = -9.8 # Aceleración negativa de la gravedad.
    h0 = 75 # Posición inicial en Y.
    x0 = 0 # Posición inicial en X.
    acvent = 0 # Aceleración negativa del viento.
    
    # Instanciar/llamar la clase.
    tirop = TiroParabolico(velinit, alpha, g, h0, x0, acvent) 
    
    # Llamar métodos.
    velx = tirop.VelocidadEnX() # Prueba unitaria, para no volver a la función sin necesidad.
    vely = tirop.VelocidadEnY()
    tmax = tirop.TiempoDeVuelo()
    xmax = tirop.AlcanceMax()
    hmax = tirop.AlturaMax()
    
    tirop.figMpA()
    
    print('Velocidad en x: {} m/s; velocidad en y: {} m/s.'.format(velx, vely))
    print('El tiempo maximo de vuelo es {} s.'.format(tmax))
    print('El alcance maximo de la trayectoria es {} m.'.format(xmax))
    print('La altura maxima de la trayectoria es {} m.'.format(hmax))
    
    # Lo correspondiente a la herencia.
    # Atributo nuevo.
    d = 50 # Distancia que se espera alcanzar.
    
    # Instanciar clase.
    tiropv = TiroParabV2(velinit, alpha, g, h0, x0, acvent, d)
    
    tiropv.angNecesarioParaD()
    tiropv.longTotalTrayec()