import numpy as np 
import sympy as sym
import matplotlib.pyplot as plt

class penduloSimple():
    
    """
    Esta clase permite dar solución al problema del péndulo simple bajo la aproximación de pequeñas oscilaciones
    Se puede resolver con el método numérico de Euler, y la solución analítica
    Permite ver la comparación entre ambos métodos de manera gráfica 
    """

    # Constructor

    def __init__(self, tiempo_inicial, tiempo_final, theta_CI, omega_CI, natural_frequency, paso_integracion):

        """
        Parametros con los que se instancia la clase

        tiempo_inicial: primer elemento del timepo en el intervalo donde se resuelve la ecuación diferencial
        tiempo_final: último elemento del timepo en el intervalo donde se resuelve la ecuación diferencial
        theta_CI: valor de theta de la condición inicial
        omega_CI: valor de omega (derivada de theta) de la condición inicial
        natural_frequency: frecuencia natural del péndulo sqrt(g / l)
        pasos_integracion: diferencia entre dos tiempos consecutivos en la integración de la ED del péndulo
        """

        # Definición de atributos de instancia 

        self.tiempo_a = tiempo_inicial
        self.tiempo_b = tiempo_final
        self.theta_CI = theta_CI
        self.w_CI = omega_CI
        self.w_0 = natural_frequency
        self.h =paso_integracion

    # Definción de métodos

    def DarArregloTiempo(self):

        return  np.arange(self.tiempo_a, self.tiempo_b + self.h, self.h)

    def Euler(self):

        # Método que implementa la solución con el métodod de Euler

        # Funciones lambda para actualizar el arreglo de los valores de theta y omega

        theta_i_plus_1 = lambda theta_i, omega_i: theta_i + self.h * omega_i

        omega_i_plus_1 = lambda omega_i, theta_i_plus_1: omega_i - self.h * (self.w_0**2) * theta_i_plus_1

        # Arreglo que contiene los tiempos sobre los cuales se integra
        tiempo_arreglo = self.DarArregloTiempo()

        # Arreglos de theta y omega que se irán actualizando
        theta_arreglo = np.array([self.theta_CI])

        omega_arreglo = np.array([self.w_CI])

        for i in range(len(tiempo_arreglo) - 1):
 
            theta_arreglo = np.append(theta_arreglo, theta_i_plus_1(theta_arreglo[i], omega_arreglo[i]))

            omega_arreglo = np.append(omega_arreglo, omega_i_plus_1(omega_arreglo[i], theta_arreglo[i + 1]))

        return theta_arreglo

    def Exacta(self):
        
        # Se definen como variables simbólicas el tiempo y la frecuencia natural 
        ts = sym.Symbol("t")
        w0s = sym.Symbol("w0")
        # Se define como función simbólica theta(t)
        theta_s = sym.Function("theta")

        # Se define la función que define la ecuación diferencial para theta
        fs = -(w0s**2) * theta_s(ts)

        # # Se resuelve la ecuación para theta
        theta_solucion_general = sym.dsolve(theta_s(ts).diff(ts, ts) - fs)
        # Se deriva theta, para tener una expresión para omega
        theta_diff_solucion_general = theta_solucion_general.rhs.diff(ts)
        theta_diff_sol_ecuacion = sym.Eq(theta_s(ts).diff(ts), theta_diff_solucion_general)

        # Se reemplazan las condiciones iniciales en las ecuaciones para theta y omega
        theta_sub_CI = theta_solucion_general.subs({theta_s(ts): self.theta_CI, ts: self.tiempo_a})
        theta_diff_sub_CI = theta_diff_sol_ecuacion.subs({theta_s(ts).diff(ts): self.w_CI, ts: self.tiempo_a})

        # Se definen como variables simbólicas las constantes de integración
        C1 = sym.Symbol("C1")
        C2 = sym.Symbol("C2")

        # Se resuelve el sistema de ecuaciones para los valores de las dos contantes
        valores_constantes = sym.solve({theta_sub_CI, theta_diff_sub_CI}, {C1, C2})
        # Se reemplazan estos valores en la solución para theta
        theta_solucion = theta_solucion_general.subs(valores_constantes)
        # Se define como función numérica
        theta_num_solucion = sym.lambdify([ts, w0s], theta_solucion.rhs, "numpy")

        tiempo_arreglo = self.DarArregloTiempo()

        theta_arreglo = theta_num_solucion(tiempo_arreglo, self.w_0).real

        return theta_arreglo
        

    def DesplazamientoAngular(self):

        theta_Euler = self.Euler()
        theta_Exacta = self.Exacta()

        tiempo_arreglo = self.DarArregloTiempo()

        # Gráfica para comparar el resultado usando ambos métodos
        fig, ax = plt.subplots(figsize=(18,15))
        ax.plot(tiempo_arreglo, theta_Euler, color='red', label = 'Solución Euler')
        ax.plot(tiempo_arreglo, theta_Exacta, color = 'blue', label = 'Solución Analítica')
        ax.set_title("Gráfica de la solución del péndulo simple en el intervalo dado", fontsize=20)
        ax.set_ylabel(r"$\theta (t)$ (rad)",fontsize=20)
        ax.set_xlabel("$t$ (s)",fontsize=20)
        ax.legend(fontsize = 18)
        ax.grid()
        fig.savefig("comparacion_pendulo_solucion.png")





