from MovParabolico import *
class movparabolicoMortero(movparabolico):
    """
    Esta clase ahora considera que nuestro objeto es un mortero que en t=0 libera su carga en la posicion
    determinada. En este instante, el proyectil sale disparado, pero por conservacion del momento la 
    carcasa tambien se acelera en direccion contraria. La magnitud de la velocidad inicial del mortero
    depende de la relacion de masas entre el proyectil y dicho mortero.

    Esta clase recibe los mismos parametros que el movimiento parabolico mas las aceleraciones en x y y
    (que ya no se ingresaran por el usuario en los metodos) y la relacion de masas.  

    Los metodos de la clase de movimiento parabolico se modifican para que devuelvan ahora arreglos con
    los calculos del proyectil y de la carcasa. Esto se hace utilizando los metodos de la clase padre en
    tanto sea posible...

    Los parametros necesarios para definir la clase son:
    - Posicion inicial en x: x0 (Numero real en SI)
    - Posicion inicial en y: y0 (Numero real en SI)
    - Magnitud de la velocidad inicial: v0 (Numero real en SI)
    - Angulo que forma la velocidad con el eje x positivo: alpha (Grados)
    - Aceleracion en x: ax (numero real en SI)
    - Aceleracion en y: ay (numero real en SI)
    - Relacion entre masa del proyectil y masa de la carcasa: mpmc (Real positivo adimensional)
    
    """
    
    # Se suman parametros de aceleraciones y relacion entre masa de proyectil y masa de carcasa
    def __init__(self, x0=0., y0=0., v0=0., alpha=0., ax= 0., ay = 0. , mpmc = 1.):
        super().__init__(x0, y0, v0, alpha)
        self.proyectil = movparabolico(x0, y0, v0, alpha)
        self.carcasa = movparabolico(x0, y0, mpmc * v0, alpha + 180)
        self.ax = ax
        self.ay = ay

    # Velocidad inicial en x:
    def v0x(self):
        return np.array([super().v0x(),self.carcasa.v0x()])

    # Velocidad inicial en y:
    def v0y(self):
        return np.array([super().v0y(),self.carcasa.v0y()])
    
    # Tiempo de vuelo:
    def flightime(self):
        return np.array([self.proyectil.flightime(self.ay), self.carcasa.flightime(self.ay)])
    
    # Posicion en x dado un tiempo:
    def x(self,t):
        return np.array([super().x(t), self.carcasa.x(t)])
    
    # Posicion en x dado un tiempo y una aceleracion en x:
    def xac(self, t):
        return np.array([super().xac(t,self.ax), self.carcasa.xac(t, self.ax)])

    # x final dado una aceleracion en y:
    def xmax(self):
        return np.array([self.proyectil.xmax(self.ay), self.carcasa.xmax(self.ay)])
    
    # x final dado una aceleracion en x y y:
    def xmaxac(self):
        return np.array([self.proyectil.xmaxac(self.ax, self.ay), self.carcasa.xmaxac(self.ax, self.ay)])
    
    # Posicion en y dado un tiempo y una aceleracion en y:
    def y(self,t):
        return np.array([super().y(t,self.ay), self.carcasa.y(t,self.ay)])

    # Altura maxima
    def ymax(self):
        return np.array([self.proyectil.ymax(self.ay), self.carcasa.ymax(self.ay)])
    
    # Arreglo de tiempos del movimiento dada una aceleracion en y:
    def arrtime(self):
        return np.array([self.proyectil.arrtime(self.ay), self.carcasa.arrtime(self.ay)])
    
    # Arreglo de posiciones en x dada una aceleracion en y:
    def arrx(self):
        return np.array([self.proyectil.arrx(self.ay), self.carcasa.arrx(self.ay)])

    # Posiciones en x durante la trayectoria dadas aceleraciones en x y en y:
    def arrxac(self):
        return np.array([self.proyectil.arrxac(self.ax, self.ay), self.carcasa.arrxac(self.ax, self.ay)])
    
    # Arreglo de posiciones en y dada una aceleracion en y:
    def arry(self):
        return np.array([self.proyectil.arry(self.ay), self.carcasa.arry(self.ay)])
    
    # Metodo que grafica la trayectoria y la guarda en un archivo .png:
    def graph(self):
        
        tfin = self.flightime()
        x = self.arrx()
        y = self.arry()
        
        plt.figure(figsize=(10,8))
        plt.plot(x[0], y[0] , label="Trayectoria proyectil")
        plt.plot(x[1], y[1] , label="Trayectoria carcasa")

        x0 = self.proyectil.x0
        y0 = self.proyectil.y0
        plt.scatter(x0, y0, label="Punto inicial", color= "black")
        
        xfin = np.array([self.proyectil.x(tfin[0]), self.carcasa.x(tfin[1])])
        yfin = np.array([self.proyectil.y(tfin[0], self.ay), self.carcasa.y(tfin[1], self.ay)])
        
        plt.scatter(xfin[0], yfin[0], label="Punto final proyectil")
        plt.scatter(xfin[1], yfin[1], label="Punto final carcasa")

        plt.grid()
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.legend()
        plt.savefig("MovParabolicoMortero.png")

    # Metodo que grafica ahora con aceleracion en x:
    def graphxac(self):
        
        tfin = self.flightime()
        x = self.arrxac()
        y = self.arry()
        
        plt.figure(figsize=(10,8))
        plt.plot(x[0], y[0] , label="Trayectoria proyectil")
        plt.plot(x[1], y[1] , label="Trayectoria carcasa")

        x0 = self.proyectil.x0
        y0 = self.proyectil.y0
        plt.scatter(x0, y0, label="Punto inicial", color = "black")
        
        xfin = np.array([self.proyectil.xac(tfin[0], self.ax), self.carcasa.xac(tfin[1], self.ax)])
        yfin = np.array([self.proyectil.y(tfin[0], self.ay), self.carcasa.y(tfin[1], self.ay)])
        
        plt.scatter(xfin[0], yfin[0], label="Punto final proyectil")
        plt.scatter(xfin[1], yfin[1], label="Punto final carcasa")

        plt.grid()
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.legend()
        plt.savefig("MovParabolicoMorteroXac.png")
