from pendulo_balistico import *
from pend_balistico_disipacion import *
if __name__ == "__main__":
    
    # Parametros para el pendulo balistico

    m = 0.1
    M = 3
    R = 1
    g = 9.81

    Pen1 = pendbalis(m, M, R, g)
    
    #Pruebas del pendulo balistico
    vel = 15
    print(f"El angulo que alcanzara el bloque con una velocidad de {vel} m/s es de {Pen1.maxangle(vel)}")
    the = 75
    print(f"La velocidad que debe llevar la bala para mover el bloque hasta un angulo {the} es {Pen1.velocity(the)}")
    
    #Grafica del pendulo balistico
    Pen1.graph(the)

    # Parametros para el pendulo balistico con disipacion
    m = 0.1
    M = 3
    R = 0#10
    g = 9.81
    alpha = 0.7

    Pen2 = pendbalisdis(m, M, R, g, alpha)

    # Pruebas del pendulo balistico con disipacion
    print(f"La velocidad minima que provoca que el bloque de vueltas para una disipacion de {alpha} es {Pen2.vminrot()}")
    vel = 15
    print(f"El angulo que alcanzara el bloque con una velocidad de {vel} m/s y disipacion de {alpha} es de {Pen2.maxangle(vel)}")
    the = 75
    print(f"La velocidad que debe llevar la bala para mover el bloque hasta un angulo {the} y disipacion de {alpha} es {Pen2.velocity(the)}")
    
    # Como se da parametro de theta final en el metodo para generar graficas este no cambia con disipacion
