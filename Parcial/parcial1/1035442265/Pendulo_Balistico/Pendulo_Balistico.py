import math
import numpy as np
import matplotlib.pyplot as plt


class PenduloBalistico():

    def __init__(self,m,M,l,h,g):

        #Atributos
        self.m=m        #Masa de la bala
        self.M =M       #Masa del bolque
        self.l=l        #longitud de la cuerda del pendulo
        self.h=h        #Elevación del pendulo en el eje vertical, nos permite calcular en angulo de desviación
        self.g =g

    def DesviacionAngulo(self):

        Ang = np.arccos((self.l-self.h)/self.l)
        return Ang
    def VelocidadBala(self):
        try:
            Vb = ((self.m+self.M)/self.m) * np.sqrt(2*self.g*self.h)
            return Vb
        except:
            print("Error verificar los parametros")

    def ArrayTime(self):
        time = np.arange(0,5,0.001)
        return time
    
    def ArrayTheta(self):
        theta= [((self.l * self.VelocidadBala())/np.sqrt(self.g/self.l)) * np.sin(np.sqrt((self.g/self.l)* i )) for i in self.ArrayTime()]
        return theta
    
    def PosX(self):
        px = [-self.l * np.sin(i) for i in self.ArrayTheta()]
        return px
    
    def PosY(self):
        py = [-self.l * np.cos(i) for i in self.ArrayTheta()]
        return py        

    def figMp(self):

        plt.figure(figsize=[10,8])
        plt.plot(self.PosX(),self.PosY())
        plt.savefig("trayectoriapendulo.png")


class PenduloBaslistico2(PenduloBalistico):

    def __init__(self, m, M, l, h, g):
        super().__init__(m, M, l, h, g)

    def Velocidadmin(self):
        Vmin = ((self.m+self.M)/self.m) * np.sqrt(2* self.g*(2*self.l))
        return Vmin
    # Polimorfismo para el metodo DesviaciónAngulo, este metodo nos permitira visualizar el ángulo de grados centigrados

    def DesviacionAngulo(self):
        Ang = np.arccos((self.l-self.h)/self.l)* (180/np.pi)
        return Ang