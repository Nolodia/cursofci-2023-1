from PenduloSimple import penduloSimple

if __name__=="__main__":

    #Configuración
    t0=0                 # límite inferior intervalo de integración (a)
    t1=5                 # límite superior intervalo de integración (b)
    theta0=1             # angulo inicial (x0)
    w0=0                 # velocidad angular inicial (y0)
    sq_gl=4            # frecuencia del oscilador
    dt=0.1               # paso de integración para el método de Euler
    n=int((t1-t0)/dt)         # cantidad de pasos para el método de Euler

    sol=penduloSimple(t0,t1,theta0,w0,n,sq_gl)

    #sol.SolAna()    
    sol.PlotPos()   
    sol.PlotVel()    