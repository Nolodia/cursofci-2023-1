from pendulobalistico import pendulo
import  matplotlib.pyplot as plt
from pendulobalistico import pendulo2
from pendulobalistico import pendulo3
import numpy as np
if __name__=='__main__':
    m=0.2 # kg 
    M= 1.5 # kg
    g=9.8
    R=9
    R1=0.5
    m1=0.2
    M1=1.5

    
    v_bloque= 15  
    sol_balistico=pendulo(m,M,R,v_bloque,g,R1,M1,m1)
    print(sol_balistico.altura()*100) # altura a la que sube el bloque muy importante
    print("el angulo que recorre la bala es de",sol_balistico.angulo(),"grados")
    sol_balastico2=pendulo2(m, M, R, v_bloque, g,R1,M1,m1)
    print("la velocidad de la bala es",sol_balastico2.velocidad_minima(),"m/s") 
    print(sol_balistico.grafica1())

    print("la velocidad minima para que de una vuelta es",sol_balistico.punto_alto_trayectoria(),"m/s")
   
    sol_balistico3=pendulo3(m,M,R,v_bloque,g,R1,M1,m1)
    print("la velocidad minima de la bala usando polimorfismo es",sol_balistico3.punto_alto_trayectoria(),"m/s")

    
