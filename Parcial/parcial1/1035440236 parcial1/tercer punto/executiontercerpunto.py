from tercer_punto import diferenciales
import  matplotlib.pyplot as plt
import numpy as np
if __name__=='__main__':
    f=lambda x: np.exp(-x)  # funcion a testear.
    xo=0
    yo=-1
    h=(1)/10
    ejecucion=diferenciales(h,f,xo,yo)
    #ejecucion.recurrencia()
    print("la solucion numerica es",ejecucion.recurrencia())
    print("la solucion analitica es",ejecucion.funcion_analica())
    print(ejecucion.grafica())
    print(ejecucion.error
          ())
    