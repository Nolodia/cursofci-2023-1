import numpy as np


#esta clase toma como partida una parabola ideal sin friccion del aire, se hereda estos atributos y se define
#una nueva clase con la friccion del aire que es la aceleracion en x
#los parametros de entrada son: gravedad, posicion inicial en y, velocidad inicial, angulo de disparo, aceleracion en x
# para la segunda clase definida se supone una aceleracion en y, 
# los parametros de entrada son: gravedad, posicion inicial en y, velocidad inicial, angulo de disparo, aceleracion en y

class parabo(): #defino clase parabola con viento en dontra

    def __init__(self,gravedad,yini,veloini,angulo,ace): #parametros a utilizar:
        #self.t = tiempo #tiempo desde su lanzamiento de la partiucla
        self.g = gravedad #gravedad del problema
        self.yo = yini #posicion inicial en y
        self.vo = veloini #velocidad inicial    
        self.grad = angulo ##angulo de disparo
        self.a = ace #aceleracion en contra solo en eje x 
    

    #métodos

    def VelocidadEnX(self):
        vx = self.vo * np.cos(np.pi/180*self.grad)  #velocidad en x
        return vx
    
    def VelocidadEnY(self):
        vy = self.vo * np.sin(np.pi/180*self.grad)  #velocidad en y
        return vy
    
    def AlcanceMax(self): #alcance maximo para la particula teninedo encuenta la aceleracion en x
        alcancemax = self.vo**2*np.sin(2*np.pi/180*self.grad)/self.g - self.a*self.TiempoVuelo()**2/2
        return alcancemax
    
    def AlturaMax(self): #altura maxima para la particula
        alturamax = self.VelocidadEnY()**2/(2*self.g)
        return alturamax
    
    def TiempoVuelo(self): #tiempo de vuelo para la particula
        tiempovuelo = 2*self.VelocidadEnY()/self.g 
        return tiempovuelo
    
    def ArrayTiempo(self): #defino array para poder apreciar la trayectoria
        t = np.linspace(0,self.TiempoVuelo(),100)
        return t

    def motionx(self):
        x = self.VelocidadEnX() * self.ArrayTiempo()  -0.5*self.a*self.ArrayTiempo()**2#defino metodo para el array en x teniendo en 
                                                                                        #cuenta la aceleracion en x
        return x

    def motiony(self):
        y = self.yo + self.VelocidadEnY() * self.ArrayTiempo() -0.5*self.g*self.ArrayTiempo()**2 #def método para el array en y
        return y

#--------------------------------------------------------------------------------------------------   
class paraboa(parabo): #heredo la clase parabola que es el caso ideal, para realizar la modificacion de la aceleracion en y, con esto tendria
       #un vector  aceleracion con componente en x y y

    def __init__(self,gravedad,yini,veloini,angulo,ace,acey): #llamo los atributos de la clase para ek contructor de la sub
        super().__init__(gravedad,yini,veloini,angulo,ace)
        self.ay = acey  #nuevo atributo solo para esta subclase, una aceleracion adicional pero en y

    def motionny(self):
        y = self.motiony() + 0.5*self.ay*self.ArrayTiempo()**2 #def método para el array en y con la nueva aceleracion en y
                                                         # haga ay potisiva si es hacia arriba y negativa si es hacia abajo el viento
        return y

    def TiempoVuelo(self):
        tiempovuelo = 2*self.VelocidadEnY()/(self.g - self.ay) #redefino el método para el tiempo de vuelo, asi cuando  llame a los métodos
                                #de la subclase, estos se evaluaran en el nuevo array de tiempo que tiene en cuenta la nueva aceleracion
        return tiempovuelo