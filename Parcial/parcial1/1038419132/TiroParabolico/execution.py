from TiroParabolico import parabo
from TiroParabolico import paraboa
import numpy as np
import matplotlib.pyplot as plt

if __name__=="__main__":

    #defino parámetros iniciales:
    g = 9.81  #gravedad [m/s2] (el signo negativo ya esta implicito en la ecuación)
    yo = 70 #posición inicial en y [m]
    vo = 20 #velocidad inicial [m/s]
    grad = 30 #angulo de disparo [°]
    ax = 0 #aceleración encontra del movimiento en x [m/s2] (sea - para derecha y + para izquierda)
    ay = 3 #aceleración en y [m/s2] (sea + para arriba y - para abajo)

    p1 = parabo(g,yo,vo,grad,ax) #heredo la clase parabola a la parabola1 (p1)
    p2 = paraboa(g,yo,vo,grad,ax,ay)#heredo la clase parabola a la parabola2 (p2)
    xp1 = p1.motionx()
    yp1 = p1.motiony() #llamo a los metodos para obtener arrays de movimiento
    yp2 = p2.motionny()
    xp2 = p2.motionx() #llamo a los metodos
    
    plt.plot(xp1,yp1,'r',label = 'p con a =  ({}, {}) m/s^2'.format(-ax,0))#grafico
    plt.plot(xp2,yp2,'b',label = 'p con a =  ({}, {}) m/s^2'.format(-ax,ay)) #grafico
    plt.legend()
    plt.title('Tiro parabólico') #poniendo linda la grafica
    plt.xlabel('Distancia [m]')
    plt.ylabel('Altura [m]')
    #plt.xlim(0) #poniendo relinda la grafica
    plt.ylim(0)
    plt.savefig('TiroParabolico.png')
    plt.show()
    

    
    
