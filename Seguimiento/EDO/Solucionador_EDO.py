import numpy as np
import matplotlib.pyplot as plt
import sympy

class EDO_numerica:

    def __init__(self,fun,x0,y0,a,b):
        print('Inicializando clase')
        self.x0 = x0
        self.y0 = y0
        self.a = a
        self.b = b
        self.fun = fun

    def Euler(self,X,Y,h,Nmax):
        for i in range(1,Nmax):
            Y.append(Y[i-1]+h*self.fun(X[i-1],Y[i-1]))
            X.append(X[i-1] + h)
    
    def Rk4(self,X,Y,h,Nmax):
        for i in range(1,Nmax):
            k1 = self.fun(X[i-1],Y[i-1])
            k2 = self.fun(X[i-1]+0.5*h,Y[i-1]+0.5*k1*h)
            k3 = self.fun(X[i-1]+0.5*h,Y[i-1]+0.5*k2*h)
            k4 = self.fun(X[i-1]+h,Y[i-1]+k3*h)
            X.append(X[i-1]+h)
            Y.append(Y[i-1]+h*(k1+2*k2+2*k3+k4)/6)

    def Sympy_Sol(self,X,Y,h,Nmax):
        xs = sympy.Symbol('x')
        ys = sympy.Function('y')
        fs = self.fun(xs,ys(xs))
        sympy.Eq(ys(xs).diff(xs),fs)
        condInit = {ys(X[0]):Y[0]}

        sol_ed = sympy.dsolve(ys(xs).diff(xs) - fs,ics= condInit)
        for i in range(1,Nmax):
            X.append(X[i-1]+h)
            Y.append(sol_ed.rhs.subs(xs,X[i]).evalf())

    def SolucionY(self,end,method = 'Euler',N=100):

        Y = [self.y0]
        X = [self.x0]
        if end <= self.b:
            h = (end-self.a)/N
        else: 
            h = (self.b-self.a)/N
        
        if method == 'Euler':
            self.Euler(X,Y,h,Nmax=N)
            return X, Y
        if method == 'RK4':
            self.Rk4(X,Y,h,Nmax=N)
            return X, Y
        if method == 'Sympy':
            self.Sympy_Sol(X,Y,h,Nmax=N)
            return X, Y
        

    def SolucionX(self,x,method = 'Euler'):
        X,Y = self.SolucionY(method = method, end=x)
        return f'y({X[-1]}) = {Y[-1]}'
    

     
    def Graf(self):
        X,Y = self.SolucionY(end=self.b)

        plt.figure()
        plt.plot(X,Y)
        plt.savefig('Grafica.png')
