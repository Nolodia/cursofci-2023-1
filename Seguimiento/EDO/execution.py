from Solucionador_EDO import *

if __name__=='__main__':
    print(f'Your name is {__name__}')

    def y_1(x,y): return -2*x+x*y

    Ej1 = EDO_numerica(y_1,1,-3,1,2)

    print(Ej1.SolucionX(1.8,method= 'RK4'))
    print(Ej1.SolucionX(1.8,method= 'Sympy'))

    Ej1.Graf()