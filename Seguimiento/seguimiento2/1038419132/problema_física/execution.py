from problema_física import Termico
import matplotlib.pyplot as plt

if __name__ == "__main__":
    dt = 20  # Observar cada 20 unidades de tiempo
    N1 = 400 # Número de partículas en el subsistema A
    N2 = 400 # Número de partículas en el subsistema B
    T1 = 100  # Temperatura inicial del subsistema A
    T2 = 20  # Temperatura inicial del subsistema B
    n = 50 # Número de observaciones

    termo = Termico(N1, N2, T1, T2)

    t = []
    T1 = []
    T2 = []
    print("tiempo \t T1 \t T2")
    for i in range(1, n):
        termo.evolucion(dt)
        print(f"{i*dt} \t {round(termo.getTemperatura1(), 2)} \t {round(termo.getTemperatura2(), 2)}")
        t.append(i*dt)
        T1.append(termo.getTemperatura1())
        T2.append(termo.getTemperatura2())

    plt.plot(t, T1, label="Subsistema A")
    plt.plot(t, T2, label="Subsistema B")
    plt.xlabel("Tiempo")
    plt.ylabel("Temperatura")
    plt.title("Evolución de la temperatura en el sistema")
    plt.xlim(t[0], t[-1])
    plt.legend()
    plt.savefig("evolucion_temperatura.png")
    plt.show()



  