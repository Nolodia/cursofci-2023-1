import math


class PenduloBalistico():

    def __init__(self,v_bala0, m_bala,m_bloque,Radio,Angle = False,g = 9.8):
        
        print('Inicializando PenduloBalistico')

        # Condiciones iniciales:
        self.v0 = v_bala0
        self.m_bala = m_bala
        self.m_bloque = m_bloque
        self.R = Radio

        # Valor por defecto
        self.g = g

        # Inicializando variables
        self.V_pendulo = 0
        self.Angle = Angle


    def VelPendulo(self):
        
        if self.m_bala != 0 or self.m_bloque != 0:
            
            self.V_pendulo = (self.m_bala*self.v0)/(self.m_bala + self.m_bloque)
        else: 
            print('Valores erróneos')

    def AnguloElevacion(self):
        self.VelPendulo()
        cos_th = 1 - (self.V_pendulo**2)/(2*self.R*self.g)
        self.Angle = math.arccos(cos_th)

    
    def Elevacion(self):
        self.AnguloElevacion()
        return 'El Péndulo Balístico tuvo un desvío de {} que corresponde a una altura de {}'.format(self.Angle,self.R*(1-math.cos(self.Angle)))



##class VelocidadMinima(PenduloBalistico):

    