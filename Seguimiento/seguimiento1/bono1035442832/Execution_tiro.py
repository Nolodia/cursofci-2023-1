

from Tiro_parabolico import tiroParabolico, tiroParabViento

"""
NOTA:
5.0

-BUEN TRABAJO: comentar las variables

"""

if __name__=='__main__':

    # Confifuración.
    velinit =100# 100
    alpha = 90
    g = -9.8
    h0 =0# 75
    x0 = 0

    # Instanciar/llamar la clase.
    tirop = tiroParabolico(velinit, alpha, g, h0, x0) 

    # Llamar métodos.
    velx = tirop.velX() # Prueba unitaria, para no volver a la función sin necesidad.
    vely = tirop.velY()
    tmax = tirop.tMaxVuelo()
    arrTime = tirop.arrTime()

    tirop.figMp()
    
    print('Velocidad en x: {} m/s; velocidad en y: {} m/s.'.format(velx, vely))
    print('El tiempo máximo de vuelo es {} s.'.format(tmax))
    print('Arreglo de tiempo de vuelo es {} s.'.format(arrTime))

    # Parte del bono. Tiro parábolico con viento.
    acvent = 0 # Aceleración negativa del viento.
    tiropv = tiroParabViento(velinit, alpha, g, h0, x0, acvent)

    tiropv.figMpV()