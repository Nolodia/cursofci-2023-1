
import math
import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico():

    def __init__(self, velinit, alpha, g, h0, x0):
        print('Inicializando clase tiroParabolico')

        #Parámetros.

        self.velinit = velinit
        self.radinalpha = math.radians(alpha)
        self.g = g
        self.h0 = h0
        self.x0 = x0

    # Métodos.
    def velX(self):
        vel_x = self.velinit*round(math.cos(self.radinalpha),3)
        return vel_x

    def velY(self):
        vel_y = self.velinit*round(math.sin(self.radinalpha),3)
        return vel_y

    def tMaxVuelo(self): # Se va a usar luego para herencias y bonificación.
        try:
            #tmax = -2*self.velY()/self.g
            tmax = (-self.velY() - np.sqrt(self.velY()**2 - 2*self.g*self.h0))/self.g
            return tmax
        except: 
            return 'Error en cálculo de tmax, revisar parámetros.'

    def arrTime(self):
        aar_time = np.arange(0,self.tMaxVuelo(), 0.001)
        return aar_time
    
    def posX(self):
        posx = [self.x0 + i*self.velX() for i in self.arrTime()]
        return posx

    def posY(self):
        posy = [self.h0 + i*self.velY() + (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy

    def figMp(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.posX(), self.posY())
        plt.title('Tiro Parábolico')
        plt.xlabel("Distancia X [m]")
        plt.ylabel("Distancia Y [m]")
        plt.grid()
        plt.savefig('Tiro_parabolico.png')

class tiroParabViento(tiroParabolico): # Esto es HERENCIA.

    def __init__(self,velinit, alpha, g, h0, x0, acevent):
        super().__init__(velinit, alpha, g, h0, x0)
        self.acvent = acevent # Aceleración causada fricción viento.

    def posXp(self): # Modificando otro método, polimorfismo.
        posxp = [self.x0 + i*self.velX() + (1/2)*self.acvent*i**2 for i in self.arrTime()]
        return posxp
    
    def figMpV(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.posXp(), self.posY())
        plt.title('Tiro Parábolico con viento')
        plt.xlabel("Distancia X [m]")
        plt.ylabel("Distancia Y [m]")
        plt.grid()
        plt.savefig('Tiro_parabolico_viento.png')

# Reto: Heredar tiro parábolico, movimiento acelerado en Y. Pero ahora tenemos viento
# en el tiro parábolico. Condición y restricción de que posición en Y no cambia; pero 
# posición en X, aplicando polimorfismo, se pone el método para calcular Y.
# Enviar gráfica de la situación.