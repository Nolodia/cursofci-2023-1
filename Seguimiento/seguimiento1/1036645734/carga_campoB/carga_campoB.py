import math
import numpy as np



class CargaCampoB():
    #atributos
    def __init__(self, q, E_k, m, alpha, t, B):
        self.q=q #carga del electrón
        self.E_k=E_k #energía cinética
        self.m=m #masa del electrón
        self.alpha=alpha*np.pi/180 #angulo de interaccion con el campo
        self.t=t #tiempo
        self.B=B #campo magnetico
        self.v=np.sqrt(2*self.E_k/self.m) #velocidad de la carga
        self.w=self.q*self.B/self.m #frecuencia angular
        self.T=2*np.pi/self.w #periodo
        self.vz=self.v*np.cos(self.alpha) #velocidad en z
        
    #metodos

    def v_xy(self): ## velocidad perperpendicular (plano xy)
        v_xy=self.v*np.sin(self.alpha)
        return v_xy


    def R(self): #radio de la particula
        R=self.v_xy()*self.m/self.q*self.B
        return R

    def posx(self): #posicion en x de la particula
        posx=self.R()*np.cos(self.w*self.t)
        return posx
    
    def posy(self): #posicion en y de la particula
        posy=self.R()*np.sin(self.w*self.t)
        return posy
    
    def posz(self): #posicion en z de la particula
        posz=self.vz*self.t
        return posz

    
