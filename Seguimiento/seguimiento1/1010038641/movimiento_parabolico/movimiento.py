import numpy as np
import matplotlib.pyplot as plt
import math


class movimiento():
    # constructor
    def __init__(self, x0, y0, v0, alfa, ay):
        print('inicializanco clase de movimiento parabólico')

    # Atributos
        self.x0 = x0
        self.y0 = y0
        self.v0 = v0
        self.alfa = math.radians(alfa) 
        self.ay = ay

    # Métodos

    # velocidad inicial en x
    def velocidad_inicial_x(self):
        return self.v0*round(np.cos(self.alfa), 3) 
    
    # velocidad inicial en y
    def velocidad_inicial_y(self):
        return self.v0*round(np.sin(self.alfa), 3)
    
    # tiempo de vuelo
    def tiempo_vuelo(self):

        if self.ay != 0:
            tv =  2*self.velocidad_inicial_y() / 9.8
            return tv
        else:
             raise Warning ('Error, la aceleración no puede ser 0') 
    

    # array de tiempo
    def arr_time(self):
        return np.arange(0, self.tiempo_vuelo(), 0.01)

    # posición en x
    def x(self):
        return [self.x0 + i*self.velocidad_inicial_x() for i in self.arr_time()]
    
    # posición en y
    def y(self):
        return [self.y0 + self.velocidad_inicial_y() * i - 0.5 * self.ay * i**2 for i in self.arr_time()]
    
    # graficación
    def graficar(self):
        f, ax = plt.subplots()
        plt.title('Movimiento Parabólico')
        ax.plot(self.x(), self.y())
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.grid()
        plt.show()
        return 


# Clase heredada para introducir una aceleración constante en x
class movimiento_modificado(movimiento):

    def __init__(self, x0, y0, v0, alfa, ay, ax):
        super().__init__(x0, y0, v0, alfa, ay)
        self.ax = ax                            # nuevo atributo para introducir el valor de la aceleración en x


    def x(self):
        if self.ax == 0:
            return super().x()                # si la aceleración en x es 0, se debe obtener el movimiento parabólico común y corriente
        else:
            self.ay = self.ax
            return super().y()                # si es diferete, se le da ese valor al método que teníamos para el movimiento acelerado en y
        
    
    def y(self):
        self.ay = 9.8                         # se redefine el movimiento en y con la aceleración de la gravedad
        return super().y()

    def graficar2(self):
        return super().graficar()             # graficamos
    