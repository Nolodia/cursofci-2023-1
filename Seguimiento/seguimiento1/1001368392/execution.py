from movcampmagnetico import *


"""
NOTA:
4.6

- Revisar los limites de en los valores de las variables
"""
if __name__ == "__main__":
    '''
    Notas: 
    - El programa supone una componente vy de cero por conveniencia. 
    - La grafica generada siempre toma el cero en el centro de curvatura del movimiento y escoge
    un y0 que permite visualizar la trayectoria de manera mas natural. 
    '''
    # Masa en eV/c2
    mass = 0#0.5e6
    # Carga en e
    charge = -1.0
    # Angulo en grados
    theta = 30
    # Energia cinetica en eV
    Ek = 18.6
    # Induccion magnetica en T
    B = 0#600.0e-6

    mov1 = movcampmagnetico(Ek, theta, mass, charge, B)

    mov1.graph()