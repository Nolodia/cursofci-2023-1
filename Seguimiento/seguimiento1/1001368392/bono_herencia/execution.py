from movparabolico import *
from acex import *

"""
NOTA
4.5

- No funciona para g=0 (control de errores)
- Conetar mejor las variables

"""
if __name__ == "__main__":
    x0 = -1
    y0 = 0
    v0 = 5
    alpha = 45
    g = -9.81
    mov1 =  movparabolico(x0, y0, v0, alpha)
    mov1.graph(-9.81)

    acx = 0
    mov1xac = movparabolicoacelx(x0, y0, v0, alpha)
    mov1xac.graphxac(acx, g)