/* tlNumber = Threshold low
 * tuNumber = Threshold Up 
 * twNumber = Time Window  
*/

void setup() {
  Serial.begin(115200); // Iniciar la comunicación serial a 115200 baudios
}

void loop() {
  if (Serial.available() > 0) {
    // Leer datos del puerto serie
    String data = Serial.readStringUntil('\n'); // Leer una línea de datos hasta el carácter de nueva línea
    data.trim(); // Eliminar espacios en blanco alrededor de los datos
    
    // Verificar si la cadena comienza con "TL" y extraer el número
    if (data.startsWith("TL")) {
      String tlNumberString = data.substring(2); // Extraer la subcadena después de "TL"
      int tlNumber = tlNumberString.toInt(); // Convertir la subcadena a entero
      int lower_dac=int(tlNumber*255.0/3300); //configura el DAC para el ThL
      dacWrite(26, lower_dac);
      Serial.print("TL Number: ");
      Serial.println(tlNumber);
    }
    
    // Verificar si la cadena comienza con "TU" y extraer el número
    if (data.startsWith("TU")) {
      String tuNumberString = data.substring(2); // Extraer la subcadena después de "TU"
      int tuNumber = tuNumberString.toInt(); // Convertir la subcadena a entero
      int upper_dac=int(tuNumber*255.0/3300); //configura el DAC para el ThU
      dacWrite(25, upper_dac); 
      Serial.print("TU Number: ");
      Serial.println(tuNumber);
    }
    
    // Verificar si la cadena comienza con "TW" y extraer el número
    if (data.startsWith("TW")) {
      String twNumberString = data.substring(2); // Extraer la subcadena después de "TW"
      int twNumber = twNumberString.toInt(); // Convertir la subcadena a entero
      Serial.print("TW Number: ");
      Serial.println(twNumber);
    }
    
 
  }
}
