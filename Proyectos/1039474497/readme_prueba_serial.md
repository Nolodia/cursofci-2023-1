****** usar junto a counter_21_04_array********

Este código utiliza Python para interactuar con un dispositivo a través de un puerto serial y recolectar datos, almacenarlos en un archivo CSV y graficarlos usando la librería Matplotlib.

La primera línea importa la librería 'serial', que permite la comunicación con dispositivos a través de un puerto serial. La segunda línea importa la librería 'matplotlib.pyplot', que se utiliza para graficar los datos. La tercera línea importa la librería 'csv', que se utiliza para escribir los datos en un archivo CSV.

Luego, se establece la conexión con el dispositivo a través del puerto serial utilizando el objeto 'serial.Serial', con el nombre del puerto serial y la velocidad de comunicación.

A continuación, se envían tres comandos al dispositivo mediante el método 'write' del objeto serial. Estos comandos son "tw100" para configurar la ventana de tiempo en 100 ms, "ts20000" que configura el tiempo de mustreo en 20 s y "start" para iniciar el barrido del PHA.

Después, se inicializan dos listas vacías 'x' y 'y'. Estas listas se utilizan para almacenar los datos que se recogen del dispositivo.

Luego, se inicia un bucle infinito, que se detendrá cuando se reciba la palabra 'finished' del dispositivo. En cada iteración, se lee una línea de datos del dispositivo utilizando el método 'readline' del objeto serial. La línea de datos se decodifica a una cadena de caracteres utilizando el método 'decode'. Se elimina cualquier espacio en blanco alrededor de la cadena utilizando el método 'strip'.

Si la cadena es igual a 'finished', se escribe los datos en un archivo CSV utilizando la librería 'csv'. El archivo se crea y se abre en modo escritura ('w') y se escribe la primera fila con los nombres de las columnas 'x' y 'y'. Luego, en un bucle for, se escriben todas las filas de datos en el archivo CSV, donde cada fila contiene el valor de x en la primera columna y el valor de y en la segunda columna. Finalmente, se grafican los datos utilizando el método 'plot' de la librería 'matplotlib.pyplot'.

Si la cadena recibida no es igual a 'finished', se divide la cadena en elementos separados por comas utilizando el método 'split'. En un bucle for, se divide cada elemento en dos valores, utilizando el asterisco como separador. Si hay dos valores, se intenta convertir cada valor a un entero utilizando el método 'int'. Si la conversión es exitosa, los valores se agregan a las listas 'x' e 'y'. Si la conversión no es exitosa, se omite el dato.

Finalmente, se cierra el objeto serial utilizando el método 'close'.